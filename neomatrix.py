#!/usr/bin/env python3
# rpi_ws281x library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.

import time
from rpi_ws281x import *
import argparse
import font5x5
import sprites5x5

# LED strip configuration:
LED_COUNT      = 120      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 32     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_STRIP = ws.SK6812_STRIP_RGBW

X_LEN = 24
Y_LEN = 5

blipfont = font5x5.loadfont()
sprites = sprites5x5.loadsprites()

# Define functions which animate LEDs in various ways.
def posmap(x, y):
    pixeloffset = 0
    if (y % 2) == 0:
        pixeloffset = (y * X_LEN) - x
    else:
        pixeloffset = x-1 + ((y-1) * X_LEN)

    #print(f'x={x} y={y} offset={pixeloffset}')
    return pixeloffset

def letter_ray(letter):
    char_ray = blipfont[letter]
    return char_ray

def placeletter(strip, char_ray, x_offset=0, fg_color=0, bg_color=0):

    if fg_color==0:
        fg_color=Color(255,0,0)
    if bg_color==0:
        bg_color=Color(0,0,0)

    #print(char_ray)
    pos=0

    for y in range(5, 0, -1):
        for x in range(x_offset, x_offset+5):
            dot=char_ray[pos]
            if dot==0:
                #print(f'pixel {x},{y} {bg_color}')
                pixwrap(strip, x, y, bg_color)
            elif dot==1:
                #print(f'pixel {x},{y} {fg_color}')
                pixwrap(strip, x, y, fg_color)
            pos += 1

def placestring(strip, string, x_offset, fg_color=0, bg_color=0):
    strlen = len(string)
    pos=0
    for x in range(x_offset, X_LEN, 6):
        #print(f'x={x} pos={pos}')
        if pos < strlen:
            thechar = string[pos]
        else:
            thechar = ' '
        placeletter(strip, letter_ray(thechar), x, fg_color, bg_color)
        pos += 1

def scrollstring(strip, string, fg_color=0, bg_color=0, scrollsleep=0.1):
    strlen = len(string)
    start_offset = X_LEN
    end_offset = 0-(strlen*6)
    for x in range(start_offset, end_offset, -1):
        quickwipe(strip)
        placestring(strip, string, x, fg_color, bg_color)
        strip.show()
        time.sleep(scrollsleep)


def pixwrap(strip, x, y, color):
    if y>Y_LEN or x>X_LEN or x<1 or y<1:
        pass
    else:
        strip.setPixelColor(posmap(x,y), color)

def leftsquare(color):
        strip.setPixelColor(posmap(1,1), color)
        strip.setPixelColor(posmap(2,1), color)
        strip.setPixelColor(posmap(3,1), color)
        strip.setPixelColor(posmap(3,1), color)
        strip.setPixelColor(posmap(3,2), color)
        strip.setPixelColor(posmap(3,3), color)
        strip.setPixelColor(posmap(3,4), color)
        strip.setPixelColor(posmap(3,5), color)
        strip.setPixelColor(posmap(2,5), color)
        strip.setPixelColor(posmap(1,5), color)
        strip.setPixelColor(posmap(1,4), color)
        strip.setPixelColor(posmap(1,3), color)
        strip.setPixelColor(posmap(1,2), color)

def quickwipe(strip, color=0, show=False):
    if color==0:
        color=Color(0,0,0)
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
    if show==True:
        strip.show()

def colorWipe(strip, color, wait_ms=50):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
        time.sleep(wait_ms/1000.0)

def theaterChase(strip, color, wait_ms=50, iterations=10):
    """Movie theater light style chaser animation."""
    for j in range(iterations):
        for q in range(3):
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i+q, color)
            strip.show()
            time.sleep(wait_ms/1000.0)
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i+q, 0)

def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)

def rainbow(strip, wait_ms=20, iterations=1):
    """Draw rainbow that fades across all pixels at once."""
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((i+j) & 255))
        strip.show()
        time.sleep(wait_ms/1000.0)

def rainbowCycle(strip, wait_ms=20, iterations=5):
    """Draw rainbow that uniformly distributes itself across all pixels."""
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((int(i * 256 / strip.numPixels()) + j) & 255))
        strip.show()
        time.sleep(wait_ms/1000.0)

def theaterChaseRainbow(strip, wait_ms=50):
    """Rainbow movie theater light style chaser animation."""
    for j in range(256):
        for q in range(3):
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i+q, wheel((i+j) % 255))
            strip.show()
            time.sleep(wait_ms/1000.0)
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i+q, 0)

# Main program logic follows:
if __name__ == '__main__':
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL, LED_STRIP)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    try:

        while True:

            for i in range(255):
                leftsquare(wheel(i))
                strip.show()


            placeletter(strip, sprites['tree'], 1, Color(255,0,0))
            placeletter(strip, sprites['diamond'], 7, Color(255,255,0), Color(0,0,0))
            placeletter(strip, sprites['tree'], 13, Color(255,0,0))
            placeletter(strip, sprites['candycane'], 19, Color(255,0,255), Color(0,0,0))
            strip.show()
            time.sleep(3)

            colorWipe(strip, Color(0,0,0), 10)

            scrollstring(strip, 'Merry Christmas')

            colorWipe(strip, Color(0,0,0), 10)

            placestring(strip, 'XMAS', 1, Color(0,255,0))
            strip.show()
            time.sleep(2)

            colorWipe(strip, Color(0,0,0), 0)

            placestring(strip, '2020', 1, Color(0,255,255))
            strip.show()
            time.sleep(3)

            for i in range(30):
                placeletter(strip, sprites['tree'], 1, Color(255,0,0), Color(255,255,255))
                placeletter(strip, sprites['tree'], 7, Color(255,0,0), Color(255,255,255))
                placeletter(strip, sprites['tree'], 13, Color(255,0,0), Color(255,255,255))
                placeletter(strip, sprites['tree'], 19, Color(255,0,0), Color(255,255,255))
                strip.show()
                time.sleep(2/(i+1))
                placeletter(strip, sprites['tree'], 1, Color(0,255,0), Color(255,255,255))
                placeletter(strip, sprites['tree'], 7, Color(0,255,0), Color(255,255,255))
                placeletter(strip, sprites['tree'], 13, Color(0,255,0), Color(255,255,255))
                placeletter(strip, sprites['tree'], 19, Color(0,255,0), Color(255,255,255))
                strip.show()
                time.sleep(2/(i+1))


            print ('Color wipe animations.')
            colorWipe(strip, Color(255, 0, 0))  # Red wipe
            colorWipe(strip, Color(0, 255, 0))  # Blue wipe
            colorWipe(strip, Color(0, 0, 255))  # Green wipe
            print ('Theater chase animations.')
            theaterChase(strip, Color(127, 127, 127), 20)  # White theater chase
            theaterChase(strip, Color(127,   0,   0), 20)  # Red theater chase
            theaterChase(strip, Color(  0,   0, 127), 20)  # Blue theater chase
            print ('Rainbow animations.')
            rainbow(strip)
            rainbowCycle(strip,1)
            theaterChaseRainbow(strip, 1)

    except KeyboardInterrupt:
        if args.clear:
            colorWipe(strip, Color(0,0,0), 10)
